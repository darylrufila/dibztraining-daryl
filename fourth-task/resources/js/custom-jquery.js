$(function() {
    $('.btn-click, .btn-reset').on('click', function() {
        var _this = $(this),
            _counter = _this.closest('.content').find('.counter'),
            _val = _counter.text();
        // increment / reset count
        _this.hasClass('btn-click') ? _counter.html(++_val) : _counter.html(0);

    });
});

(function test() {
    // test javascript function;
})();
