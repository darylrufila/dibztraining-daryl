<!DOCTYPE html>
<html>
<head>
	<title>RUFILA | FOURTH TASK</title>
	<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/style.css">
    <link href="resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700|Raleway:400,700" rel="stylesheet">
</head>
<body>

	<div class="container">
		<div class="col-sm-12">
			<div class="section header col-sm-12 container">
				<div class="row">
					<p class="txt-title">
						CAT CLICKER
					</p>
				</div>
			</div>

        	<?php
				for ($x = 0; $x < 3; $x++) :
					$url = "resources/img/cat" . $x . ".gif";
			?>
			<div class="content">
				<div class="section main col-sm-6">
					<div class="row">
						<img src="<?php echo $url?>" class="img-responsive col-sm-6 img-cat">
						<p class="txt">
							COUNTER:
						</p>
						<p class="counter txt-count">0</p>
						<button class="btn btn-info btn-section col-sm-offset-2 col-sm-8 btn-click">
							Click Cat!
						</button>
						<button class="btn btn-info btn-section col-sm-offset-2 col-sm-8 btn-reset">
							Reset Cat
						</button>
					</div>
				</div>
			</div>
			<?php
				endfor;
			?>

		</div>
	</div>

	<script src="resources/bootstrap/js/bootstrap.min.js"></script>
	<script src="resources/js/jquery-3.2.1.min.js"></script>
	<script src="resources/js/custom-jquery.js"></script>
</body>
</html>
